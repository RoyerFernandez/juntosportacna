<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Afiliados</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Afiliados</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>Nuestros Afiliados</h2>
            <h3></h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 " style="text-align: justify;"> 
            <p>
             El Movimiento Regional “JUNTOS POR TACNA”, se integra por ciudadanos con principios éticos, morales y democráticos con alta vocación de servicio que fomenta el desarrollo integral y articulado de la región Tacna, absorbe y analiza las demandas sociales para construir estrategias y ofertas sostenibles; el MOVIMIENTO REGIONAL “JUNTOS POR TACNA” de manera transparente y democrática, involucra y fortalece la participación activa de la sociedad civil
            </p>
            
            <p class="font-italic">
              <button class="btn btn-success"> <i class="bi bi-text-indent-left "></i>  Relación de Todos Nuestros Afiliados </button>
            </p>
            <br>

           
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>