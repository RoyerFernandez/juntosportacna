<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Simbolo</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Simbolo</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-4">
            <h2>Simbolo</h2>
            <h3></h3>
          </div>

          <div class="col-lg-2 col-md-6 mt-4 mt-lg-0 ">
            <div class="box">
              <h3> </h3>
              <h4><sup><div class="btn-wrap">
                <img width="150px" height="150px" src="assets/img/simbolo.jpg">
              </div></sup></i><span> </span></h4>
              <ul>
               	 
              </ul>
             
            </div>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" style="text-align: justify;">
            <p>
              El símbolo del Movimiento Regional “JUNTOS POR TACNA” es una pelota de colores rojo y blanco. Se ubica sobre un fondo verde con borde rojo, de esquinas ovaladas.
            </p>

          </div>



        </div>

      </div>
    

  


 


      </div>
    </section><!-- End Pricing Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>