<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>IDEARIO</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Ideario</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>Ideario</h2>
            <h3></h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" style="text-align: justify;">
            <p>
              El Movimiento Regional “JUNTOS POR TACNA”, se integra por ciudadanos con principios éticos, morales y democráticos con alta vocación de servicio que fomenta el desarrollo integral y articulado de la región Tacna, absorbe y analiza las demandas sociales para construir estrategias y ofertas sostenibles; el MOVIMIENTO REGIONAL “JUNTOS POR TACNA” de manera transparente y democrática, involucra y fortalece la participación activa de la sociedad civil
            </p>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <section id="pricing" class="pricing">
      <div class="container">

        <div class="row">

  
          <div class="col-lg-6 col-md-6 mt-4 mt-lg-0 offset-lg-3">
            <div class="box">
              <h3>Ideario </h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="documentos/ideario.pdf" target="_blank" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>

 

        </div>

      </div>
    </section><!-- End Pricing Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>