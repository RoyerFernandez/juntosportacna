<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Conócenos</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Conócenos</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>Conócenos</h2>
            <h3></h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
              El Movimiento Regional “JUNTOS POR TACNA”, se integra por ciudadanos con principios éticos, morales y democráticos con alta vocación de servicio que fomenta el desarrollo integral y articulado de la región Tacna, absorbe y analiza las demandas sociales para construir estrategias y ofertas sostenibles; el MOVIMIENTO REGIONAL “JUNTOS POR TACNA” de manera transparente y democrática, involucra y fortalece la participación activa de la sociedad civil
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Provincia: Tacna</li>
              <li><i class="ri-check-double-line"></i> Provincia: Tarata</li>
              <li><i class="ri-check-double-line"></i> Provincia: Candarave</li>
              <li><i class="ri-check-double-line"></i> Provincia: Jorge Basadre</li>
            </ul>
            <p class="font-italic">
              Dirección de nuestro Local Principal 
            </p>
            <br>

            Tacna - Ciudad Nueva   <br>
                Calle Sin Nombre N° 123 <br><br>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>