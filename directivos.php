<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Directivos</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Directivos</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->



    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Directivos</h2>
          <p>Nuestros Principales Dirigentes</p>
          <button class="btn btn-success"> <i class="bi bi-text-indent-left "></i>  Relación de Todos Nuestros Directivos </button>
        </div>

        <div class="row">
          <!-- 
          <div class="col-lg-6">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="#" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Walter White</h4>
                <span>Secretaría General</span>
                <p>Explicabo voluptatem mollitia et repellat</p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="#" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Sarah Jhonson</h4>
                <span>Personero Legal</span>
                <p>Aut maiores voluptates amet et quis</p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="assets/img/team/team-3.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>William Anderson</h4>
                <span>Personero Legal</span>
                <p>Quisquam facilis cum velit laborum corrupti</p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Amanda Jepson</h4>
                <span>Personero Legal</span>
                <p>Dolorum tempora officiis odit laborum officiis</p>
                
              </div>
            </div>
          </div>
  
        </div>
           -->
      </div>
    </section>
     


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>