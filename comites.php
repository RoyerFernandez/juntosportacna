<?php
require('header.php');
?>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Comités</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Comités</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>Nuestros Comités</h2>
            <h3></h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
              Sólo a través de la construcción de nuestros comités vamos a poder trascender las coyunturas, apoyar con nuestro trabajo y movilización en las grandes causas de Tumbes pero sin perder de vista nuestros objetivos y lo más importante, sin extender cheques en blanco a la nefasta burocracia partidaria. Recuerda: Organización debe ser independencia, no sometimiento ni utilización.
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Provincia: Tacna</li>
              <li><i class="ri-check-double-line"></i> Provincia: Tarata</li>
              <li><i class="ri-check-double-line"></i> Provincia: Candarave</li>
              <li><i class="ri-check-double-line"></i> Provincia: Jorge Basadre</li>
            </ul>
            <p class="font-italic">
              Dirección de nuestro Local Principal 
            </p>
            <br>

            Tacna - Ciudad Nueva   <br>
                Calle Sin Nombre N° 123 <br><br>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>