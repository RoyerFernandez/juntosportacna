<?php
require('header.php');
?>
  <main id="main">
  

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Normativa</h2>
          <ol>
            <li><a href="index.html">Inicio</a></li>
            <li>Normativa</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->
    <!-- ======= Breadcrumbs ======= -->
    
  <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

        <div class="row">

  
          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>Estatuto </h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>Reglamento</h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>Codigo de Ética</h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>Reglamento de Organización y Funciones</h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>


          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>ACTA DE LEVANTAMIENTO DE OBSERVACIONES AL ESTATUTO</h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="documentos/ACTA DE LEVANTAMIENTO DE OBSERVACIONES AL ESTATUTO.pdf" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>ACTA DE LEVANTAMIENTO DE OBSERVACIONES AL REGLAMENTO ELECTORAL</h3>
              <h4><sup></sup><i class="bi bi-file-earmark-text-fill"></i><span> </span></h4>
              <ul>
               
              </ul>
              <div class="btn-wrap">
                <a href="documentos/ACTA DE LEVANTAMIENTO DE OBSERVACIONES AL REGLAMENTO ELECTORAL.pdf" class="btn-buy">VER MÁS</a>
              </div>
            </div>
          </div>



        </div>

      </div>
    </section><!-- End Pricing Section -->


    

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 <?php
require('footer.php');
?>