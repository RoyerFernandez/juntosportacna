<?php
require('header.php');
?>

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

      <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

      <div class="carousel-inner" role="listbox">

        <!-- Slide 1 -->
        <div class="carousel-item active" style="background-image: url(assets/img/slide/juntos.jpg)"> 
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">MOVIMIENTO REGIONAL <br> JUNTOS <span>POR TACNA</span></h2>
              <p class="animate__animated animate__fadeInUp"></p>
              <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Conocer Más</a>
            </div>
          </div>
        </div>

        <!-- Slide 2 -->
        <div class="carousel-item" style="background-image: url(assets/img/slide/juntos.jpg)">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Movimiento Regional</h2>
              <p class="animate__animated animate__fadeInUp"></p>
              <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Conocer Más</a>
            </div>
          </div>
        </div>

        <!-- Slide 3 -->
        <div class="carousel-item" style="background-image: url(assets/img/slide/juntos.jpg)">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Juntos Por Tacna</h2>
              <p class="animate__animated animate__fadeInUp"></p>
              <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Conocer Más</a>
            </div>
          </div>
        </div>

      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>NUESTRO COMPROMISO</h2>
            <h3>    </h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" style="text-align: justify;">
            <p>
                        El Movimiento Regional “JUNTOS POR TACNA”, se integra por ciudadanos con principios éticos, morales y democráticos con alta vocación de servicio que fomenta el desarrollo integral y articulado de la región Tacna, absorbe y analiza las demandas sociales para construir estrategias y ofertas sostenibles; el MOVIMIENTO REGIONAL “JUNTOS POR TACNA” de manera transparente y democrática, involucra y fortalece la participación activa de la sociedad civil  </p>
            
            <p class="font-italic">
              
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    

    

  </main><!-- End #main -->

 <?php
require('footer.php');
?>